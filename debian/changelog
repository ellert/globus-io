globus-io (12.4-3) unstable; urgency=medium

  * Enable bind-now hardening

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 06 Mar 2024 18:31:03 +0100

globus-io (12.4-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062164

 -- Steve Langasek <vorlon@debian.org>  Wed, 28 Feb 2024 18:33:43 +0000

globus-io (12.4-2) unstable; urgency=medium

  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 22:28:51 +0200

globus-io (12.4-1) unstable; urgency=medium

  * New GCT release v6.2.20220524
  * Drop patches included in the release

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 25 May 2022 22:13:22 +0200

globus-io (12.3-2) unstable; urgency=medium

  * Use sha256 hash when generating test certificates

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 13 May 2022 09:18:18 +0200

globus-io (12.3-1) unstable; urgency=medium

  * Use -nameopt sep_multiline to derive certificate subject string
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop ancient Replaces/Conflicts/Breaks

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 15 Dec 2020 13:04:13 +0100

globus-io (12.2-1) unstable; urgency=medium

  * Fix test race condition
  * Drop patch globus-io-fix-test-race-condition.patch

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Mon, 02 Sep 2019 21:12:43 +0200

globus-io (12.1-3) unstable; urgency=medium

  * Fix test race condition

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 30 Jul 2019 10:59:43 +0200

globus-io (12.1-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:46 +0200

globus-io (12.1-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release (12.0)
  * Use 2048 bit RSA key for tests (12.1)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:32 +0200

globus-io (11.10-1) unstable; urgency=medium

  * GT6 update: Use 2048 bit keys to support openssl 1.1.1
  * Drop patch globus-io-2048-bits.patch (acceptedupstream)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sat, 01 Sep 2018 21:03:20 +0200

globus-io (11.9-3) unstable; urgency=medium

  * Use 2048 bit RSA key for tests
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 26 Aug 2018 14:18:22 +0200

globus-io (11.9-2) unstable; urgency=medium

  * Migrate to dbgsym packages
  * Support DEB_BUILD_OPTIONS=nocheck

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:35 +0200

globus-io (11.9-1) unstable; urgency=medium

  * GT6 update: Remove legacy SSLv3 support

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 25 Jun 2017 18:21:59 +0200

globus-io (11.8-1) unstable; urgency=medium

  * GT6 update

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 10 Nov 2016 22:18:20 +0100

globus-io (11.7-1) unstable; urgency=medium

  * GT6 update: Updates for OpenSSL 1.1.0
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 01 Sep 2016 15:55:17 +0200

globus-io (11.5-1) unstable; urgency=medium

  * GT6 update: Fix uninitialized variable reads and some warnings in io test

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 03 May 2016 18:59:28 +0200

globus-io (11.4-2) unstable; urgency=medium

  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 16:17:34 +0100

globus-io (11.4-1) unstable; urgency=medium

  * GT6 update (test improvements)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 29 May 2015 20:29:56 +0200

globus-io (11.3-1) unstable; urgency=medium

  * GT6 update (test fixes)
  * Set GLOBUS_HOSTNAME during make check

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 20 Feb 2015 14:54:12 +0100

globus-io (11.2-1) unstable; urgency=medium

  * GT6 update
  * Drop patch globus-io-tests-localhost.patch (fixed upstream)
  * Enable verbose tests

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 15 Nov 2014 13:50:35 +0100

globus-io (10.12-1) unstable; urgency=medium

  * GT6 update
  * Drop patch globus-io-doxygen.patch (obsolete)
  * Remove documentation package again (upstream changed their minds)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 27 Oct 2014 11:43:28 +0100

globus-io (10.11-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata
  * Enable checks
  * Add documentation package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 23 Sep 2014 17:18:23 +0200

globus-io (9.5-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.5
  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 10 Nov 2013 20:56:50 +0100

globus-io (9.4-2) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 17:42:24 +0200

globus-io (9.4-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.4

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 20 Feb 2013 14:08:13 +0100

globus-io (9.3-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.1
  * Drop patch globus-io-deps.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 29 Apr 2012 12:18:20 +0200

globus-io (9.2-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 19:00:57 +0100

globus-io (9.2-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.0
  * Drop patch globus-io-parens.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 28 Dec 2011 17:02:58 +0100

globus-io (6.3-9) unstable; urgency=low

  * Add README file
  * Add missing dependencies

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Apr 2011 14:07:56 +0200

globus-io (6.3-8) unstable; urgency=low

  * Converting to package format 3.0 (quilt)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:30 +0200

globus-io (6.3-7) unstable; urgency=low

  * Update to Globus Toolkit 5.0.0
  * Add debug package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 27 Jan 2010 07:25:09 +0100

globus-io (6.3-6) unstable; urgency=low

  * Fix suggest parentheses compilator warning.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 04 Jun 2009 01:47:00 +0200

globus-io (6.3-5) unstable; urgency=low

  * Fix rule dependencies in the debian/rules file.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 13 May 2009 15:30:03 +0200

globus-io (6.3-4) unstable; urgency=low

  * Fixing inherited dependency of binary to package that
    was removed from the archive.

 -- Steffen Moeller <moeller@debian.org>  Thu, 30 Apr 2009 15:20:55 +0200

globus-io (6.3-3) unstable; urgency=low

  * Initial release (Closes: #514479).
  * Rebuilt to correct libltdl dependency.
  * Preparing for other 64bit platforms than amd64.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 18 Apr 2009 20:17:33 +0200

globus-io (6.3-2) UNRELEASED; urgency=low

  * Only quote the Apache-2.0 license if necessary.
  * Updated deprecated Source-Version in debian/control.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 26 Mar 2009 09:21:25 +0100

globus-io (6.3-1) UNRELEASED; urgency=low

  * First build.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 03 Jan 2009 11:16:06 +0100
